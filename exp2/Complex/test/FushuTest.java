import junit.framework.TestCase;
import org.junit.Test;

public class FushuTest extends TestCase {
    Fushu.Complex sq = new Fushu.Complex(1.0, 4.0);
    Fushu.Complex gst = new Fushu.Complex(1.0, 3.0);
    @Test
    public void testAdd() {
        assertEquals("(2.0+7.0i)", sq.ComplexAdd(gst).toString());
    }

    @Test
    public void testSub() {
        assertEquals("(0.0+1.0i)", sq.ComplexSub(gst).toString());
    }

    @Test
    public void testMul() {
        assertEquals("(-11.0+7.0i)", sq.ComplexMulti(gst).toString());
    }

    @Test
    public void testDiv() {
        assertEquals("(4.0-0.0i)", sq.ComplexDiv(gst).toString());
    }
}
