import java.util.Objects;
public class Cputest {
    public static void main(String args[]) {
        int x = 2200, y = 200;
        CPU cpu = new CPU();
        HardDisk disk = new HardDisk();
        PC pc = new PC();
        cpu.setSpeed(x);
        disk.setAmount(y);
        pc.setCPU(cpu);
        pc.setHardDisk(disk);
        pc.show();
    }
}
    class CPU {
        int v;
        CPU(){
            System.out.println("CPU内的构造方法调用");
        }
        void setSpeed(int m){
            v=m;
        }
        int getSpeed(){
            return v;
        }

        @Override
        public String toString() {
            return "CPU{" +
                    "v=" + v +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CPU cpu = (CPU) o;
            return v == cpu.v;
        }

        @Override
        public int hashCode() {

            return Objects.hash(v);
        }
    }
    class HardDisk {
        int amount;
        HardDisk(){
            System.out.println("HardDisk里面的构造方法调用");
        }
        void setAmount(int m){
            amount=m;
        }
        int getAmount(){
            return amount;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            HardDisk hardDisk = (HardDisk) o;
            return getAmount() == hardDisk.getAmount();
        }

        @Override
        public String toString() {
            return "HardDisk{" +
                    "amount=" + amount +
                    '}';
        }
    }
    class PC {
        CPU cpu;
        HardDisk HD;
        PC(){
            System.out.println("PC里面的构造方法调用");
        }
        void setCPU(CPU cpu){
            this.cpu=cpu;
        }
        void setHardDisk(HardDisk HD){
            this.HD=HD;
        }
        void show(){
            System.out.println("速度："+cpu.getSpeed());
            System.out.println("容量："+HD.getAmount());
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            PC pc = (PC) o;
            return Objects.equals(cpu, pc.cpu) &&
                    Objects.equals(HD, pc.HD);
        }

        @Override
        public int hashCode() {

            return Objects.hash(cpu, HD);
        }

        @Override
        public String toString() {
            return "PC{" +
                    "cpu=" + cpu +
                    ", HD=" + HD +
                    '}';
        }
    }



