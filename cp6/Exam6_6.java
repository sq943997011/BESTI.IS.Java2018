public class Exam6_6 {
   public static void main(String args[]) {
      AdvertisementBoard board = new AdvertisementBoard();
      board.show(new BlackLandCorp());
      board.show(new WhiteCloudCorp());
   }
}
