import java.util.Scanner;
public class Fibonacci {
    public static void main(String[] args) {
        System.out.println("请输入你想要求几个数");
        Scanner reader=new Scanner(System.in);
        int i = reader.nextInt();
        int a=1,b=0,c=0,sum=0;
        if(i<1) {
            System.out.println("非法情况，输入个数小于1");
        }
        else
        {
            for(int n=1;n<=i;n++) {
                sum = a + b;
                c = b;
                b = a;
                a = sum;
                System.out.println(sum);
            }
        }
        System.out.println("Fibonacci函数值为"+sum+"     取值个数为"+i);
    }
}
