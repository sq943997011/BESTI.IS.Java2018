import org.junit.Test;
import junit.framework.TestCase;
public class StringBufferDemoTest extends TestCase {
        StringBuffer sq = new StringBuffer("gst and sq");
        StringBuffer gst = new StringBuffer("sq and gst lueluelue hahaha");
    public void testcharat() {
        assertEquals('g', sq.charAt(0));
        assertEquals('s', gst.charAt(0));
    }
    public void testcapacity() {
        assertEquals(26, sq.capacity());
        assertEquals(43, gst.capacity());
    }
    public void testlength() {
        assertEquals(10, sq.length());
        assertEquals(27, gst.length());
    }
    public void testindexof() {
        assertEquals(0, sq.indexOf("gst"));
        assertEquals(11, gst.indexOf("lue"));
    }
}